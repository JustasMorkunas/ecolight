<?php


if (!isset($_GET['option']))
    exit("Option parameter is required!");

if (!in_array($_GET['option'], ['year','month','day']))
    exit("Unknown option");

$mysqli = (new MysqlClient())->instance();
$query = "";
switch ($_GET['option']){
    case 'year':
        $query = "SELECT DISTINCT(YEAR(date)) AS options FROM `history`";
        break;
    case 'month':
        $query = "SELECT DISTINCT(MONTH(date)) AS options FROM `history`";
        break;
    case 'day':
        $query = "SELECT DISTINCT(DAY(date)) AS options FROM `history`";
        break;
}

$options = $mysqli->query($query)->fetch_all(MYSQLI_ASSOC);
$options = $options[0];
echo json_encode($options['options']);

