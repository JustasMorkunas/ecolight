package com.justas.ecolight;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.justas.ecolight.DTO.Light;
import com.justas.ecolight.contracts.HttpObserverSubject;
import com.justas.ecolight.database.LightsDatabaseHandler;
import com.justas.ecolight.httpRequests.LightHttpSignal;

public class LightElement extends LinearLayout implements HttpObserverSubject<Boolean>
{

    private TextView lightNameEl;
    private TextView lightPowerEl;
    private ImageView lightImg;
    private Switch aSwitch;

    protected Light light;

    private Context context;
    private AttributeSet attributeSet;
    private LightsDatabaseHandler dbHandler;

    public LightElement(Context context, AttributeSet attributeSet)
    {
        super(context, attributeSet);
        this.context = context;
        this.attributeSet = attributeSet;
        init();
    }

    private void init()
    {
        inflate(context, R.layout.light_item, this);
        initComponents();
        setSwitchOnClickListener();
        setLightNameOnClickListener();
    }

    private void initComponents()
    {
        lightNameEl = findViewById(R.id.light_name);
        lightPowerEl = findViewById(R.id.ligth_power);
        lightImg = findViewById(R.id.lightElImg);
        aSwitch = findViewById(R.id.lightSwitch);
        dbHandler = new LightsDatabaseHandler(getContext());
    }

    private void setSwitchOnClickListener()
    {
        aSwitch.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                LightHttpSignal lightHttpSignal = new LightHttpSignal();
                lightHttpSignal.attach(LightElement.this);
                lightHttpSignal.execute("http://jusmor.stud.if.ktu.lt/?controller=post&lightNr="+light.getLightNr()+"&lightId="+light.getId());
            }
        });
    }

    private void setLightNameOnClickListener()
    {
        lightNameEl.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent editLightAction = new Intent(context.getApplicationContext(), CreateLightActivity.class);
                editLightAction.putExtra("LightObj", light);
                context.startActivity(editLightAction);
            }
        });
    }

    public void setLight(Light light)
    {
        this.light = light;
        lightNameEl.setText(this.light.getName());
        lightPowerEl.setText(String.valueOf(this.light.getPower())+"W");
        aSwitch.setChecked(light.getStatus());
        if(light.getStatus())
            lightImg.setImageResource(R.drawable.ic_lightbulb_on);
        else
            lightImg.setImageResource(R.drawable.ic_lightbulb);
    }


    protected void turnOnTheLight()
    {
        lightImg.setImageResource(R.drawable.ic_lightbulb_on);
        light.setStatus(true);
        dbHandler.updateLight(light);
    }

    protected void turnOffTheLight()
    {
        lightImg.setImageResource(R.drawable.ic_lightbulb);
        light.setStatus(false);
        dbHandler.updateLight(light);
    }

    @Override
    public void update(Boolean operationStatus)
    {
        if(operationStatus)
        {
            if(light.getStatus())
                turnOffTheLight();
            else
                turnOnTheLight();
        }
        else
            aSwitch.setChecked(light.getStatus());

    }
}
