package com.justas.ecolight;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.justas.ecolight.DTO.Light;
import com.justas.ecolight.database.LightsDatabaseHandler;
import com.justas.ecolight.httpRequests.LightHttpSignal;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;


public class CreateLightActivity extends BaseWithMenuActivity {

    protected Light light;
    LightsDatabaseHandler dbHandler;

    private EditText lightNameInput;
    private EditText lightPowerInput;
    private Spinner lightNumberInput;
    private Toolbar toolbar;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_new_light_main);
        initMenu();
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void init()
    {
        setProperties();
        enterLightValuesToFields();
        setSpinnerOptions();
        bindActionOnSaveButton();
    }

    private void setProperties()
    {
        lightNameInput = findViewById(R.id.inputLightName);
        lightPowerInput = findViewById(R.id.inputLightPower);
        lightNumberInput = findViewById(R.id.inputLightNumber);
        toolbar = findViewById(R.id.toolbar);
        dbHandler = new LightsDatabaseHandler(this);
    }

    private void enterLightValuesToFields()
    {
        Light lightObj = (Light) getIntent().getSerializableExtra("LightObj");
        if(lightObj != null)
        {
            this.light = lightObj;
            lightNameInput.setText(light.getName());
            lightPowerInput.setText(String.valueOf(light.getPower()));
            addDeleteButtonToToolbar();
        }
    }

    private void addDeleteButtonToToolbar()
    {
        ImageView btn = new ImageView(getApplicationContext());
        btn.setImageResource(R.drawable.ic_delete);
        btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        Toolbar.LayoutParams l1 = new Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.WRAP_CONTENT);
        l1.gravity = Gravity.END;
        l1.setMargins(16,16,36,16);
        btn.setLayoutParams(l1);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(light.getStatus())
                    (new LightHttpSignal()).execute("http://jusmor.stud.if.ktu.lt/?controller=post&lightNr="+light.getLightNr()+"&lightId="+light.getId());
                dbHandler.deleteLight(light);
                finish();
            }
        });
        toolbar.addView(btn);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void setSpinnerOptions()
    {
        Spinner spinner = findViewById(R.id.inputLightNumber);
        List<Integer> options = new LinkedList<>();
        for (int i = 1; i < 10; i++)
            options.add(i);
        options.removeAll(dbHandler.getLightNumbers());

        if(this.light != null && !options.contains(this.light.getLightNr()))
        {
            options.add(this.light.getLightNr());
            options.sort(new Comparator<Integer>()
            {
                @Override
                public int compare(Integer first, Integer second)
                {
                    return first.compareTo(second);
                }
            });
        }

        List<String> spinnerOptions = new LinkedList<>();

        for(int i = 0; i < options.size(); i++)
                spinnerOptions.add("      "+String.valueOf(options.get(i)+1));

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, spinnerOptions);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        if(this.light != null)
        {
            int selectedElement = options.indexOf(this.light.getLightNr());
            spinner.setSelection(selectedElement);
        }
    }

    private void bindActionOnSaveButton()
    {
        Button button = findViewById(R.id.inputNewLightSaveButton);
        button.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                if(validateFields())
                {
                    if(light == null){
                        String selectedLightNr = lightNumberInput.getSelectedItem().toString().trim();
                        light = new Light(lightNameInput.getText().toString(), Integer.parseInt(lightPowerInput.getText().toString()), false, Integer.parseInt(selectedLightNr) - 1);
                        dbHandler.addLightToDb(light);
                    } else {
                        if(light.getStatus()) {
                            (new LightHttpSignal()).execute("http://jusmor.stud.if.ktu.lt/?controller=post&lightNr=" + light.getLightNr() + "&lightId=" + light.getId());
                            light.setStatus(false);
                        }
                        String selectedLightNr = lightNumberInput.getSelectedItem().toString().trim();
                        light.setName(lightNameInput.getText().toString())
                                .setPower(Integer.parseInt(lightPowerInput.getText().toString()))
                                .setLightNr(Integer.valueOf(selectedLightNr)-1);
                        dbHandler.updateLight(light);
                    }

                    finish();
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private boolean validateFields()
    {
        boolean isValid = true;
        lightNameInput.setBackgroundTintList(ColorStateList.valueOf(Color.BLACK));
        lightPowerInput.setBackgroundTintList(ColorStateList.valueOf(Color.BLACK));
        if(lightNameInput.getText().toString().isEmpty())
        {
            lightNameInput.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
            isValid = false;
        }

        if(lightPowerInput.getText().toString().isEmpty()){
            lightPowerInput.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
            isValid = false;
        }
        return isValid;
    }

}
