package com.justas.ecolight.contracts;

public interface HttpObserverSubject<T>
{
    void update(T operationStatus);
}
