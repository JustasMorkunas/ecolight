<!DOCTYPE html>
<html>
<head>

</head>
<body>
    <div class="container">
        <div class="item-wrapper">
            <div class="item-header">
                <h2>Electricity calculator</h2>
            </div>
            <div class="item-body">
                <?php for($i = 0; $i < 10; $i++){ ?>
                    <form style="display: inline" action="/">
                        <input type="hidden" name="controller" value="post"/>
                        <input type="submit" name="light" style="background-color: <?php echo ($getModel->getCurrentLightStatus($i) == 1) ? "green" : "red" ?>;" value="<?php echo $i ?>"/>
                    </form>
                <?php } ?>
            </div>
        </div>
    </div>
</body>
</html>