<?php

class MysqlClient{

    /**
     * @var mysqli
     */
    protected $mysql;

    public function instance()
    {
        if(!$this->mysql)
            $this->initConnection();
        return $this;
    }

    private function initConnection()
    {
        $this->mysql = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_DATABASE);
        if ($this->mysql->connect_errno) {
            echo "Failed to connect to MySQL: (" . $this->mysql->connect_errno . ") " . $this->mysql->connect_error;
            die();
        }
    }

    public function query($query)
    {
        return $this->mysql->query($query);
    }

}