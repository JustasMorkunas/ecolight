package com.justas.ecolight;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.ValueDependentColor;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.Series;
import com.justas.ecolight.DTO.FilterData;
import com.justas.ecolight.DTO.Light;
import com.justas.ecolight.contracts.HttpObserverSubject;
import com.justas.ecolight.database.LightsDatabaseHandler;
import com.justas.ecolight.httpRequests.EnergyConsumptionHttpSignal;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class StatisticsActivity extends BaseWithMenuActivity implements HttpObserverSubject<Long>
{

    protected Spinner selectYearSpinner;
    protected Spinner selectMonthSpinner;
    protected Spinner selectDaySpinner;
    protected List<Light> lights;
    protected GraphView graph;
    protected FilterData filterData;
    protected int updatedLights;


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistics_main);
        initMenu();
        initProperties();
        initSpinners();
        initializeRequest();
    }

    protected void initProperties()
    {
        this.selectYearSpinner = findViewById(R.id.statisticsYear);
        this.selectMonthSpinner = findViewById(R.id.statisticsMonth);
        this.selectDaySpinner = findViewById(R.id.statisticsDay);
        this.graph = findViewById(R.id.graph);
        LightsDatabaseHandler handler = new LightsDatabaseHandler(this);
        lights = handler.getLights();
        filterData = new FilterData();
        updatedLights = 0;
    }

    protected void initSpinners()
    {
        setYearSpinnerOptions();
        setMonthSpinnerOptions();
        setDaySpinnerOptions();
        initYearSpinnerOnChangeListener();
        initMonthSpinnerOnChangeListener();
        initDaySpinnerOnChangeListener();
    }

    private void setYearSpinnerOptions()
    {
        String[] years = {"---", "2019"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, years);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectYearSpinner.setAdapter(adapter);
    }

    private void setMonthSpinnerOptions()
    {
        String[] monthOptions;
        if(filterData.getYear() != null) {
            DateFormatSymbols dateFormatSymbols = new DateFormatSymbols();
            String[] months = dateFormatSymbols.getShortMonths();
            monthOptions = new String[(months.length + 1)];
            monthOptions[0] = "---";
            for (int i = 0; i < months.length; i++)
                monthOptions[i + 1] = months[i];
        } else {
            monthOptions = new String[]{"---"};
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, monthOptions);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectMonthSpinner.setAdapter(adapter);
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
    }

    private void setDaySpinnerOptions()
    {
        String[] days;
        if(filterData.getMonth() != null && filterData.getYear() != null) {
            Calendar c = Calendar.getInstance();
            c.set(filterData.getYear(), filterData.getMonth() - 1, 1);
            int monthMaxDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);

            days = new String[monthMaxDays + 1];
            days[0] = "---";
            for (int i = 1; i <= monthMaxDays; i++)
                days[i] = String.valueOf(i);
        } else {
            days = new String[]{"---"};
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, days);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectDaySpinner.setAdapter(adapter);

    }

    private void initYearSpinnerOnChangeListener()
    {
        selectYearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if(position == 0)
                    filterData.setYear(null);
                else
                    filterData.setYear(Integer.parseInt(selectYearSpinner.getSelectedItem().toString().trim()));
                setMonthSpinnerOptions();
                setDaySpinnerOptions();
                initializeRequest();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){ }
        });
    }

    private void initMonthSpinnerOnChangeListener()
    {
        selectMonthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if(position == 0)
                    filterData.setMonth(null);
                else
                    filterData.setMonth(position);
                setDaySpinnerOptions();
                initializeRequest();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });
    }

    private void initDaySpinnerOnChangeListener()
    {
        selectDaySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if(position == 0)
                    filterData.setDay(null);
                else
                    filterData.setDay(position);
                initializeRequest();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });
    }

    private void initializeRequest()
    {
        for(Light light: lights)
        {
            EnergyConsumptionHttpSignal signal = new EnergyConsumptionHttpSignal();
            signal.attach(light);
            signal.attach(StatisticsActivity.this);
            signal.execute(getRequestUrl(light));
        }
    }

    private String getRequestUrl(Light light)
    {
        String filters = "";
        if(filterData.getYear() != null)
            filters += "&year="+filterData.getYear();
        if(filterData.getMonth() != null)
            filters += "&month="+filterData.getMonth();
        if(filterData.getDay() != null)
            filters += "&day="+filterData.getDay();
        return "http://jusmor.stud.if.ktu.lt/?controller=getHistory&lightId="+light.getId()+filters;
    }


    protected void initGraph()
    {
        this.graph.removeAllSeries();
        updatedLights = 0;
        DataPoint[] dataPoints = new DataPoint[lights.size()];
        Random rand = new Random();

        for(int i = 0; i < lights.size(); i++)
        {
            dataPoints[i] = new DataPoint((i+1), lights.get(i).getElectricityConsumption());
        }

        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(dataPoints);
        series.setValueDependentColor(new ValueDependentColor<DataPoint>()
        {
            @Override
            public int get(DataPoint data)
            {
                return Color.rgb((int) data.getX()*255/4, (int) Math.abs(data.getY()*255/6), 100);
            }
        });


        series.setDataWidth(0.5);
        this.graph.addSeries(series);
        this.graph.getViewport().setMinX(0);
        this.graph.getViewport().setMaxX(11);

        this.graph.getViewport().setXAxisBoundsManual(true);

        series.setDrawValuesOnTop(true);
        series.setValuesOnTopColor(Color.BLACK);

        series.setOnDataPointTapListener(new OnDataPointTapListener()
        {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint)
            {
                Toast toast = Toast.makeText(getApplicationContext(),lights.get((int)dataPoint.getX() - 1).getName(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL, 0, 0);
                toast.show();
            }
        });
    }


    @Override
    public void update(Long operationStatus)
    {
        updatedLights++;
        if(updatedLights == lights.size())
            initGraph();
    }
}
