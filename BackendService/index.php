<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

date_default_timezone_set(DateTimeZone::listIdentifiers(DateTimeZone::EUROPE)[54]);

require_once './libs/mysql.php';
require './config.php';

if(isset($_GET['controller']))
{
    include('./contoller/'.$_GET['controller'].'.php');
} else {
    include('./contoller/home.php');
}