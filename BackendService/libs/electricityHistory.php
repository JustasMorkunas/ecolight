<?php
include_once './libs/Light.php';
class electricityHistory {
    /**
     * @var MysqlClient
     */
    private $mysqli;
    private $lightId;

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var Light[]
     */
    private $turnedOn = array();

    /**
     * @var Light[]
     */
    private $turnedOff = array();

    public function __construct($lightId, Filter $filter) {
        $this->mysqli = (new MysqlClient())->instance();
        $this->filter = $filter;
        $this->lightId = $lightId;
        $this->parseLightHistory();

    }

    public function getDifference() {
        $diffInSeconds = 0;
        $j = 0;
        if(sizeof($this->turnedOff) < 1 || sizeof($this->turnedOn) < 1)
            return 0;

        if($this->turnedOn[0]->date > $this->turnedOff[0]->date)
            $j = 1;

        for($i = 0; $i < sizeof($this->turnedOn); $i++, $j++){
            if (isset($this->turnedOff[$j]))
                $diffInSeconds += intval($this->turnedOff[$j]->date->diff($this->turnedOn[$i]->date)->format('%s'));
        }

        return $diffInSeconds;
    }

    private function parseLightHistory() {
        $queryResult = $this->mysqli->query($this->getSqlQuery(1));
        $resultsOn = array();
        while ($item = $queryResult->fetch_assoc())
            $resultsOn[] = $item;

        if (!empty($resultsOn)) {
            foreach($resultsOn as $item) {
                $this->turnedOn[] = new Light($item['id'], $item['lightNr'], $item['lightId'], $item['status'], $item['date']);
            }
        }

        $queryResult = $this->mysqli->query($this->getSqlQuery(0));
        $resultsOff = array();
        while ($item = $queryResult->fetch_assoc())
            $resultsOff[] = $item;

        if (!empty($resultsOff)) {
            foreach ($resultsOff as $item) {
                $this->turnedOff[] = new Light($item['id'], $item['lightNr'], $item['lightId'], $item['status'], $item['date']);
            }
        }
    }

    private function getSqlQuery($status) {
        $query = "SELECT * FROM history WHERE lightId = {$this->lightId} AND status={$status}";
        if($this->filter->getYear() != null)
            $query .= " AND YEAR(date) = {$this->filter->getYear()}";

        if($this->filter->getMonth() != null)
            $query .= " AND MONTH(date) = {$this->filter->getMonth()}";

        if($this->filter->getDay() != null)
            $query .= " AND DAY(date) = {$this->filter->getDay()}";

        $query .= " ORDER BY date ASC";
        return $query;
    }
}