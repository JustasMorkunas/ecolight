package com.justas.ecolight;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.LinearLayout;

import com.justas.ecolight.DTO.Light;
import com.justas.ecolight.database.LightsDatabaseHandler;

import java.util.ArrayList;

public class MainActivity extends BaseWithMenuActivity {

    LightsDatabaseHandler dbHandler;
    FloatingActionButton fab;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initMenu();

        dbHandler = new LightsDatabaseHandler(this);
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCreateLightAction();
            }
        });

        fillLightItems();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        LinearLayout container = findViewById(R.id.lightItemsContainer);
        container.removeAllViews();
        fillLightItems();
    }

    private void openCreateLightAction()
    {
        Intent createNewLightActivity = new Intent(getApplicationContext(), CreateLightActivity.class);
        startActivity(createNewLightActivity);
    }

    private void fillLightItems()
    {
        LinearLayout container = findViewById(R.id.lightItemsContainer);
        ArrayList<Light> lights = this.dbHandler.getLights();
        for(Light light : lights){
            LightElement lightEl = new LightElement(getApplicationContext(), null);
            lightEl.setLight(light);
            container.addView(lightEl);
        }
        if(lights.size() == 10)
            fab.hide();
        else
            fab.show();
    }
}
