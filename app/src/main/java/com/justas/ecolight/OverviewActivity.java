package com.justas.ecolight;

import android.graphics.Color;
import android.os.Bundle;

import com.justas.ecolight.DTO.Light;
import com.justas.ecolight.contracts.HttpObserverSubject;
import com.justas.ecolight.database.LightsDatabaseHandler;
import com.justas.ecolight.httpRequests.EnergyConsumptionHttpSignal;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;

public class OverviewActivity extends BaseWithMenuActivity implements HttpObserverSubject<Long>
{

    protected PieChartView pieChartView;
    protected List<Light> lights;
    private int updatedLights;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.overview_main);
        initMenu();
        initParams();
        initializeRequest();
    }

    private void initializeRequest()
    {
        for(Light light : lights)
        {
            EnergyConsumptionHttpSignal signal = new EnergyConsumptionHttpSignal();
            signal.attach(light);
            signal.attach(OverviewActivity.this);
            signal.execute("http://jusmor.stud.if.ktu.lt/?controller=getHistory&lightId="+light.getId());
        }
    }

    protected void initParams()
    {
        LightsDatabaseHandler dbHandler = new LightsDatabaseHandler(this);
        this.pieChartView = findViewById(R.id.pie_chart);
        this.lights = dbHandler.getLights();
        updatedLights = 0;
    }

    protected void initPieChart()
    {

        List<SliceValue> pieData = new ArrayList<>();
        for(int i = 0; i < lights.size(); i++)
        {
            pieData.add(new SliceValue((float) lights.get(i).getElectricityConsumption(), getColor(lights.get(i).getLightNr(), i)).setLabel(lights.get(i).getName()));
        }

        PieChartData pieChartData = new PieChartData(pieData);
        pieChartData.setHasLabels(true).setValueLabelTextSize(14);
        pieChartData.setHasCenterCircle(true).setCenterText1(getResources().getString(R.string.overview_cicle_center_text)).setCenterText1FontSize(20);

        pieChartView.setPieChartData(pieChartData);
    }

    private int getColor(int x, int y)
    {
        return Color.rgb((int) x*255/4, (int) Math.abs(y*255/6), 100);
    }

    @Override
    public void update(Long response)
    {
        updatedLights++;
        if(lights.size() == updatedLights)
            initPieChart();
    }
}
