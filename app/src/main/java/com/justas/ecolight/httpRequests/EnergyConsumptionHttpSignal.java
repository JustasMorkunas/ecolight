package com.justas.ecolight.httpRequests;

import android.os.AsyncTask;
import android.util.Log;

import com.justas.ecolight.contracts.HttpObserver;
import com.justas.ecolight.contracts.HttpObserverSubject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

public class EnergyConsumptionHttpSignal extends AsyncTask<String, Void, Long> implements HttpObserver
{

    private List<HttpObserverSubject> subjects;

   public EnergyConsumptionHttpSignal()
   {
    super();
    subjects = new LinkedList<>();
   }

    @Override
    protected Long doInBackground(String... strings)
    {
        try
        {
            URL url = new URL(strings[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream input = urlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(input));
            String line = bufferedReader.readLine();
            Long timeInSecs = Long.parseLong(line.trim());
            Log.d("HTTP RESPONSE", line);
            urlConnection.disconnect();
            updateSubjects(timeInSecs);
            return timeInSecs;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return 0l;
    }

    private void updateSubjects(Long timeInSecs)
    {
        for(HttpObserverSubject subject : subjects)
        {
            subject.update(timeInSecs);
        }
    }

    @Override
    public void attach(HttpObserverSubject subject)
    {
        subjects.add(subject);
    }
}
