
const byte numLEDs = 10;
byte ledPin[numLEDs] = {13, 12, 11, 10, 9, 8, 7, 6, 5, 4};
char ledStatus[numLEDs] = {'0', '0', '0', '0', '0', '0', '0', '0', '0', '0'};

const byte buffSize = 40;
char inputBuffer[buffSize];
const char startMarker = '<';
const char endMarker = '>';
byte bytesRecvd = 0;
boolean readInProgress = false;
boolean newDataFromPC = false;

char messageFromPC[buffSize] = {0};

unsigned long curMillis;

unsigned long prevReplyToPCmillis = 0;
unsigned long replyToPCinterval = 1000;

//=============

void setup() {
  Serial.begin(9600);
  
    // flash LEDs so we know we are alive
  for (byte n = 0; n < numLEDs; n++) {
     pinMode(ledPin[n], OUTPUT);
     digitalWrite(ledPin[n], HIGH);
  }
  delay(500); // delay() is OK in setup as it only happens once
  
  for (byte n = 0; n < numLEDs; n++) {
     digitalWrite(ledPin[n], LOW);
  }
  
  
    // tell the PC we are ready
  Serial.println("<Arduino is ready>");
}

//=============

void loop() {
  curMillis = millis();
  getDataFromPC();
  replyToPC();
}

//=============

void getDataFromPC() {

    // receive data from PC and save it into inputBuffer
    
  if(Serial.available() > 0) {

    char x = Serial.read();

      // the order of these IF clauses is significant
      
    if (x == endMarker) {
      readInProgress = false;
      newDataFromPC = true;
      inputBuffer[bytesRecvd] = 0;
      parseData();
    }
    
    if(readInProgress) {
      inputBuffer[bytesRecvd] = x;
      bytesRecvd ++;
      if (bytesRecvd == buffSize) {
        bytesRecvd = buffSize - 1;
      }
    }

    if (x == startMarker) { 
      bytesRecvd = 0; 
      readInProgress = true;
    }
  }
}

void parseData() {
    
  char * strtokIndx; // this is used by strtok() as an index
  for(byte i = 0; i < numLEDs; i++){
    if(i == 0){
      strtokIndx = strtok(inputBuffer,",");      // get the first part - the string      
    } else {
      strtokIndx = strtok(NULL, ",");      
    }  

    updateLed(i, strtokIndx);
  }
}

void updateLed(byte index, String data){
  if(data.indexOf("0") >= 0){
    digitalWrite(ledPin[index], LOW);
    ledStatus[index] = '0';
  } else {
    digitalWrite(ledPin[index], HIGH);
    ledStatus[index] = '1';  
  }
}

void replyToPC() {

  if (newDataFromPC) {
    newDataFromPC = false;
    Serial.print("<Msg ");
    Serial.print("LED status: ");
    Serial.print(ledStatus);
    Serial.print(" Time ");
    Serial.print(curMillis >> 9); // divide by 512 is approx = half-seconds
    Serial.println(">");
  }
}
