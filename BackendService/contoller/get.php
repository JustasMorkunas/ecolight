<?php

class getModel
{
    private $table = 'history';
    private $lightsCount = 10;
    private $mysql;

    public function __construct()
    {
        $this->mysql = (new MysqlClient())->instance();
    }

    public function getAllLightStatusString()
    {
        $statuses = [];
        for($i = 0; $i < $this->lightsCount; $i++){
            $statuses[] = $this->getCurrentLightStatus($i);
        }
        return "<".join(",", $statuses).">";
    }

    public function getCurrentLightStatus($light)
    {
        $res = $this->mysql->query("SELECT * FROM {$this->table} WHERE lightNr={$light} ORDER BY date DESC LIMIT 1");
        if($res->num_rows > 0)
            return (int)$res->fetch_object()->status;
        return 0;
    }
}


$getModel = new getModel();

print $getModel->getAllLightStatusString();