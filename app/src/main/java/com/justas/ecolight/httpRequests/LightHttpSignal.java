package com.justas.ecolight.httpRequests;

import android.os.AsyncTask;
import android.util.Log;

import com.justas.ecolight.contracts.HttpObserver;
import com.justas.ecolight.contracts.HttpObserverSubject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

public class LightHttpSignal extends AsyncTask<String, Void, Boolean> implements HttpObserver
{

    List<HttpObserverSubject> subjects;

    public LightHttpSignal()
    {
        super();
        subjects = new LinkedList<>();
    }

    @Override
    protected Boolean doInBackground(String... strings)
    {
        try
        {
            URL url = new URL(strings[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream input = urlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(input));
            String line = bufferedReader.readLine();
            Log.d("HTTP RESPONSE", line);
            urlConnection.disconnect();
            return jsonAnalyzer(line);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return false;
    }

    private boolean jsonAnalyzer(String jsonString)
    {
        try{
            JSONObject jsonObject = new JSONObject(jsonString);
            String queryStatus = jsonObject.getString("status");
            if(queryStatus.trim().equalsIgnoreCase("OK"))
                return true;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean result)
    {
        super.onPostExecute(result);
        informSubjects(result);
    }

    private void informSubjects(boolean operationStatus)
    {
        for (HttpObserverSubject subject: subjects)
            subject.update(operationStatus);
    }

    @Override
    public void attach(HttpObserverSubject subject)
    {
        if(subject != null)
            subjects.add(subject);
    }
}
