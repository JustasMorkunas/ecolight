<?php

$mysql = (new MysqlClient)->instance();
if(isset($_GET['lightNr']) && isset($_GET['lightId']))
{
    $prev_status = $mysql->query("SELECT * FROM history WHERE lightNr={$_GET['lightNr']} AND lightId={$_GET['lightId']} ORDER BY date DESC LIMIT 1");

    if($prev_status->num_rows > 0){
        $prev_status = $prev_status->fetch_object()->status;

    } else {
        $prev_status = 0;
    }
    $new_stat = 0;
    if($prev_status == 0)
        $new_stat = 1;

    $mysql->query("INSERT INTO `history`(`lightNr`, `lightId`, `status`, `date`) VALUES ({$_GET['lightNr']}, {$_GET['lightId']}, {$new_stat}, NOW())");
    echo json_encode(['status' => 'OK', 'newStatus' => $new_stat]);
} else {
    echo  json_encode(['status' => 'error']);
}



