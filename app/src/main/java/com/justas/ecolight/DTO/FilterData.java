package com.justas.ecolight.DTO;

public class FilterData
{

    private Integer year;
    private Integer month;
    private Integer day;

    public Integer getYear()
    {
        return year;
    }

    public FilterData setYear(Integer year)
    {
        this.year = year;
        return this;
    }

    public Integer getMonth()
    {
        return month;
    }

    public FilterData setMonth(Integer month)
    {
        this.month = month;
        return this;
    }

    public Integer getDay()
    {
        return day;
    }

    public FilterData setDay(Integer day)
    {
        this.day = day;
        return this;
    }
}
