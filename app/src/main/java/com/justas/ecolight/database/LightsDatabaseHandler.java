package com.justas.ecolight.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.justas.ecolight.DTO.Light;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class LightsDatabaseHandler extends SQLiteOpenHelper
{

    private static final int DATABASE_VERSION = 8;

    private static final String DATABASE_NAME = "ecolightDatabase.db";

    private static final String TABLE_LIGHTS = "lights";

    private static final String KEY_ID = "id";
    private static final String KEY_LIGHT_NUMBER = "lightNr";
    private static final String KEY_LIGHT_NAME = "name";
    private static final String KEY_POWER = "power";
    private static final String KEY_STATUS = "status";

    private static final int LIGHT_ID_COL_ID = 0;
    private static final int LIGHT_NUMBER_COL_ID = 1;
    private static final int LIGHT_NAME_COL_ID = 2;
    private static final int LIGHT_POWER_COL_ID = 3;
    private static final int LIGHT_STATUS_COL_ID = 4;

    public LightsDatabaseHandler(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String CREATE_LIGHTS_TABLE = "CREATE TABLE "+ TABLE_LIGHTS + "("
                + KEY_ID +" INTEGER PRIMARY KEY, "
                + KEY_LIGHT_NUMBER +" INTEGER, "
                + KEY_LIGHT_NAME + " TEXT, "
                + KEY_POWER + " INTEGER, "
                + KEY_STATUS + " INTEGER "
                + ")";
        Log.d("Database", CREATE_LIGHTS_TABLE);
        db.execSQL(CREATE_LIGHTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LIGHTS );
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        onUpgrade(db, oldVersion, newVersion);
    }

    public long addLightToDb(Light light)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_LIGHT_NUMBER, light.getLightNr());
        values.put(KEY_LIGHT_NAME, light.getName());
        values.put(KEY_POWER, light.getPower());
        values.put(KEY_STATUS, light.getStatus() ? 1 : 0);

        long idValue = db.insert(TABLE_LIGHTS, null, values);
        db.close();
        return  idValue;
    }

    public void updateLight(Light light)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(KEY_LIGHT_NUMBER, light.getLightNr());
        cv.put(KEY_LIGHT_NAME, light.getName());
        cv.put(KEY_POWER, light.getPower());
        cv.put(KEY_STATUS, light.getStatus());

        Log.d("Values", cv.toString());
        db.update(TABLE_LIGHTS, cv, KEY_ID + " = ?", new String[] { String.valueOf(light.getId()) });
        db.close();
    }

    public void deleteLight(Light light)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_LIGHTS, KEY_ID + " = ?", new String[] {String.valueOf(light.getId())});
        db.close();
    }

    public ArrayList<Light> getLights()
    {
        ArrayList<Light> lights = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM "+TABLE_LIGHTS;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst())
        {
            do {
                lights.add(getLightElement(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return lights;
    }

    protected Light getLightElement(Cursor cursor)
    {
        return  new Light(
                cursor.getInt(LIGHT_ID_COL_ID),
                cursor.getString(LIGHT_NAME_COL_ID),
                cursor.getInt(LIGHT_POWER_COL_ID),
                (cursor.getInt(LIGHT_STATUS_COL_ID) == 1 ? true : false),
                cursor.getInt(LIGHT_NUMBER_COL_ID)
                );
    }

    public List<Integer> getLightNumbers()
    {
        List<Integer> numbers = new LinkedList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM "+TABLE_LIGHTS;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst())
        {
            do {
                numbers.add(cursor.getInt(LIGHT_NUMBER_COL_ID));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return numbers;
    }

}
