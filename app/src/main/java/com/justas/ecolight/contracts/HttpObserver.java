package com.justas.ecolight.contracts;

public interface HttpObserver
{
    void attach(HttpObserverSubject subject);
}
