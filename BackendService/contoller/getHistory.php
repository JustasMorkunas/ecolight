<?php

if (!isset($_GET['lightId'])) {
    exit( "Please provide lightId");
}

include_once './libs/electricityHistory.php';
include_once './libs/Filter.php';

$filter = new Filter();
if (isset($_GET['year']) && !empty($_GET['year']))
    $filter->setYear(intval($_GET['year']));

if (isset($_GET['month']) && !empty($_GET['month']))
    $filter->setMonth(intval($_GET['month']));

if (isset($_GET['day']) && !empty($_GET['day']))
    $filter->setDay(intval($_GET['day']));

echo (new electricityHistory($_GET['lightId'], $filter))->getDifference();