
def sendToArduino(sendStr):
    ser.write(sendStr.encode())


def recvFromArduino():
    global startMarker, endMarker
    
    ck = ""
    x = "z" # any value that is not an end- or startMarker
    byteCount = -1 # to allow for the fact that the last increment will be one too many
    
    # wait for the start character
    while    ord(x) != startMarker: 
        x = ser.read()
    
    # save data until the end marker is found
    while ord(x) != endMarker:
        if ord(x) != startMarker:
            ck = ck + str(x)[2]
            byteCount += 1
        x = ser.read()
        
    
    return ck


#============================

def waitForArduino():
    global startMarker, endMarker
        
    msg = ""
    while msg.find("Arduino is ready") == -1:

        while ser.inWaiting() == 0:
            pass
                
        msg = recvFromArduino()

        print (msg)
        print

def runTest():
    waitingForReply = False
    n = 0
    while True:
        msg = getLightStatuses()
        if waitingForReply == False:
            sendToArduino(msg)
            print ("Sent from PC -- LOOP NUM " + str(n) + " TEST STR " + msg)
            waitingForReply = True
            n = n + 1

        if waitingForReply == True:

            while ser.inWaiting() == 0:
                pass
                
            dataRecvd = recvFromArduino()
            print ("Reply Received    " + dataRecvd)
            waitingForReply = False

            print ("===========")

        time.sleep(0.02)
        
def getLightStatuses():
    contents = urllib.request.urlopen("http://jusmor.stud.if.ktu.lt/?controller=get").read()
    contents = contents.decode('utf-8')
    return contents


#======================================

# THE DEMO PROGRAM STARTS HERE

#======================================

import serial
import time
import urllib.request

print
print

serPort = "COM5"
baudRate = 9600
ser = serial.Serial(serPort, baudRate)
print ("Serial port " + serPort + " opened    Baudrate " + str(baudRate))


startMarker = 60
endMarker = 62


waitForArduino()

runTest()

ser.close

