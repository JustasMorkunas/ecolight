<?php



class Light
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $lightNr;

    /**
     * @var int
     */
    public $lightId;

    /**
     * @var bool
     */
    public $status;

    /**
     * @var DateTime
     */
    public $date;

    /**
     * Light constructor.
     * @param string $id
     * @param string $lightNr
     * @param string $lightId
     * @param string $status
     * @param string $date
     */
    public function __construct($id, $lightNr, $lightId, $status, $date)
    {
        $this->id = intval($id);
        $this->lightNr = intval($lightNr);
        $this->lightId = intval($lightId);
        $this->status = ($status == "1") ? true : false;
        $this->date = new DateTime($date);
    }

}