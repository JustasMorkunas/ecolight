package com.justas.ecolight;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

public class BaseWithMenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener
{

    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.bar_chart_statistics) {
            this.openStatisticsActivity();
        } else if (id == R.id.home_menu_item) {
            this.openMainActivity();
        } else if (id == R.id.bar_chart_overview){
            this.openOverviewActivity();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    protected void initMenu()
    {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    protected void openStatisticsActivity()
    {
        if(!this.getClass().getSimpleName().equalsIgnoreCase("MainActivity")) {
            finish();
        }
        Intent statisticsIntent = new Intent(getApplicationContext(), StatisticsActivity.class);
        startActivity(statisticsIntent);
    }

    protected void openMainActivity()
    {
        if(!this.getClass().getSimpleName().equalsIgnoreCase("MainActivity")) {
            finish();
        }
    }

    protected void openOverviewActivity()
    {
        if(!this.getClass().getSimpleName().equalsIgnoreCase("MainActivity")) {
            finish();
        }
        Intent statisticsIntent = new Intent(getApplicationContext(), OverviewActivity.class);
        startActivity(statisticsIntent);
    }
}
