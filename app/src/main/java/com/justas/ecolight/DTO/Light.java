package com.justas.ecolight.DTO;

import com.justas.ecolight.contracts.HttpObserverSubject;

import java.io.Serializable;

public class Light implements Serializable, HttpObserverSubject<Long> {

    protected String name;
    protected int id;
    protected int power;
    protected int lightNr;
    protected boolean status;
    protected long timeActive;

    public Light(int id, String name, int power, boolean status, int lightNr)
    {
        this.id = id;
        this.name = name;
        this.power = power;
        this.status = status;
        this.lightNr = lightNr;
    }

    public Light(String name, int power, boolean status, int lightNr)
    {
        this.id = -1;
        this.name = name;
        this.power = power;
        this.status = status;
        this.lightNr = lightNr;
    }

    public int getId() { return this.id; }

    public String getName() { return this.name; }

    public int getPower() { return this.power; }

    public boolean getStatus() {return  this.status; }

    public int getLightNr() { return this.lightNr; }

    public long getTimeActive() {return this.timeActive; }

    public double getElectricityConsumption() {
        return (double) power * ((double) timeActive/3600);
    }

    public Light setName(String name) {
        this.name = name;
        return this;
    }

    public Light setPower(int power) {
        this.power = power;
        return this;
    }

    public Light setStatus(boolean status) {
        this.status = status;
        return this;
    }

    public Light setLightNr(int nr)
    {
        this.lightNr = nr;
        return this;
    }

    public Light setTimeActive(long timeActive)
    {
        this.timeActive = timeActive;
        return this;
    }

    @Override
    public void update(Long timeActive)
    {
        this.timeActive = timeActive;
    }
}
